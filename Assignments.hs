module Assignments where

import Data.Map as M
import Control.Monad.Reader
import Control.Monad.State
import AbsData
import Expressions
import Absgramatyka

interpretAssi :: Assignment -> Semantics ()
interpretAssi (Assign ident expr) = do
  val <- evalEx expr
  Just loc <- asks (M.lookup (evalIdent ident))
  modify (M.insert loc val)
interpretAssi (AOper ident OpAPlus expr) = do
  val1 <- evalEx expr
  Just loc <- asks (M.lookup (evalIdent ident))
  Just val2 <- gets (M.lookup loc)
  modify (M.insert loc (val1+val2))
interpretAssi (AOper ident OpAMinus expr) = do
  val1 <- evalEx expr
  Just loc <- asks (M.lookup (evalIdent ident))
  Just val2 <- gets (M.lookup loc)
  modify (M.insert loc (val1-val2))
interpretAssi (AOper ident OpAMult expr) = do
  val1 <- evalEx expr
  Just loc <- asks (M.lookup (evalIdent ident))
  Just val2 <- gets (M.lookup loc)
  modify (M.insert loc (val1*val2))
interpretAssi (AOper ident OpADiv expr) = do
  val1 <- evalEx expr
  Just loc <- asks (M.lookup (evalIdent ident))
  Just val2 <- gets (M.lookup loc)
  modify (M.insert loc (div val2 val1))
interpretAssi (AIncrDecr ident Increment) = do
  Just loc <- asks (M.lookup (evalIdent ident))
  Just val <- gets (M.lookup loc)
  modify (M.insert loc (val+1))
interpretAssi (AIncrDecr ident Decrement) = do
  Just loc <- asks (M.lookup (evalIdent ident))
  Just val <- gets (M.lookup loc)
  modify (M.insert loc (val-1))