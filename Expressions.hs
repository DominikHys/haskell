module Expressions where

import Data.Map as M
import Control.Monad.Reader
import Control.Monad.State
import Absgramatyka
import AbsData

evalEx :: Expression -> Semantics Integer
evalEx (Exprs e) = do
  eval e

eval :: Expr -> Semantics Integer
eval (EOr exp1 exp2) = do
  val1 <- eval exp1
  val2 <- eval exp2
  if val1 /= 0 || val2 /= 0 then return 1 else return 0
eval (EAnd exp1 exp2) = do
  val1 <- eval exp1
  val2 <- eval exp2
  if val1 /= 0 && val2 /= 0 then return 1 else return 0
eval (EEq exp1 exp2) = do
  val1 <- eval exp1
  val2 <- eval exp2
  if val1 == val2 then return 1 else return 0
eval (ENEq exp1 exp2) = do
  val1 <- eval exp1
  val2 <- eval exp2
  if val1 /= val2 then return 1 else return 0
eval (ELt exp1 exp2) = do
  val1 <- eval exp1
  val2 <- eval exp2
  if val1 < val2 then return 1 else return 0
eval (EGt exp1 exp2) = do
  val1 <- eval exp1
  val2 <- eval exp2
  if val1 > val2 then return 1 else return 0
eval (EAdd exp1 exp2) = do
  val1 <- eval exp1
  val2 <- eval exp2
  return $ val1 + val2
eval (ESub exp1 exp2) = do
  val1 <- eval exp1
  val2 <- eval exp2
  return $ val1 - val2
eval (EMul exp1 exp2) = do
  val1 <- eval exp1
  val2 <- eval exp2
  return $ val1 * val2
eval (EDiv exp1 exp2) = do
  val1 <- eval exp1
  val2 <- eval exp2
  return $ (div val1 val2)
eval (ENeg exp1) = do
  val1 <- eval exp1
  return $ (-1) * val1
eval (EVar var) = do
  --Just loc <- asks (M.lookup (evalIdent var))
  loc <- takeLocation var
  --Just val <- gets (M.lookup loc)
  val <- takeValue loc
  return val
eval (EInt i) = return i
eval (ETrue) = return 1
eval (EFalse) = return 0