module Program where

import Data.Map as M
import Control.Monad.Reader
import Control.Monad.State
import AbsData
import Absgramatyka
import Assignments
import Declarations
import Expressions
import Statements

interpretProgram :: Program -> Semantics ()
interpretProgram (Prog decls block) = do
    env' <- evalDeclarations decls
    local (const env') (interpretBl block)

execProgram :: Program -> IO ()
execProgram p = do
    finalState <- execStateT (runReaderT (interpretProgram p) emptyEnv) initialSt
    print finalState -- tutaj zastąp przez return ()