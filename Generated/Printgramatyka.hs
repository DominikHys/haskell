{-# OPTIONS -fno-warn-incomplete-patterns #-}
module Printgramatyka where

-- pretty-printer generated by the BNF converter

import Absgramatyka
import Data.Char


-- the top-level printing method
printTree :: Print a => a -> String
printTree = render . prt 0

type Doc = [ShowS] -> [ShowS]

doc :: ShowS -> Doc
doc = (:)

render :: Doc -> String
render d = rend 0 (map ($ "") $ d []) "" where
  rend i ss = case ss of
    "["      :ts -> showChar '[' . rend i ts
    "("      :ts -> showChar '(' . rend i ts
    "{"      :ts -> showChar '{' . new (i+1) . rend (i+1) ts
    "}" : ";":ts -> new (i-1) . space "}" . showChar ';' . new (i-1) . rend (i-1) ts
    "}"      :ts -> new (i-1) . showChar '}' . new (i-1) . rend (i-1) ts
    ";"      :ts -> showChar ';' . new i . rend i ts
    t  : "," :ts -> showString t . space "," . rend i ts
    t  : ")" :ts -> showString t . showChar ')' . rend i ts
    t  : "]" :ts -> showString t . showChar ']' . rend i ts
    t        :ts -> space t . rend i ts
    _            -> id
  new i   = showChar '\n' . replicateS (2*i) (showChar ' ') . dropWhile isSpace
  space t = showString t . (\s -> if null s then "" else (' ':s))

parenth :: Doc -> Doc
parenth ss = doc (showChar '(') . ss . doc (showChar ')')

concatS :: [ShowS] -> ShowS
concatS = foldr (.) id

concatD :: [Doc] -> Doc
concatD = foldr (.) id

replicateS :: Int -> ShowS -> ShowS
replicateS n f = concatS (replicate n f)

-- the printer class does the job
class Print a where
  prt :: Int -> a -> Doc
  prtList :: [a] -> Doc
  prtList = concatD . map (prt 0)

instance Print a => Print [a] where
  prt _ = prtList

instance Print Char where
  prt _ s = doc (showChar '\'' . mkEsc '\'' s . showChar '\'')
  prtList s = doc (showChar '"' . concatS (map (mkEsc '"') s) . showChar '"')

mkEsc :: Char -> Char -> ShowS
mkEsc q s = case s of
  _ | s == q -> showChar '\\' . showChar s
  '\\'-> showString "\\\\"
  '\n' -> showString "\\n"
  '\t' -> showString "\\t"
  _ -> showChar s

prPrec :: Int -> Int -> Doc -> Doc
prPrec i j = if j<i then parenth else id


instance Print Integer where
  prt _ x = doc (shows x)


instance Print Double where
  prt _ x = doc (shows x)


instance Print Ident where
  prt _ (Ident i) = doc (showString ( i))
  prtList es = case es of
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString ",") , prt 0 xs])



instance Print Program where
  prt i e = case e of
   Prog decls block -> prPrec i 0 (concatD [prt 0 decls , doc (showString "main") , prt 0 block])


instance Print Arg where
  prt i e = case e of
   Args type' id -> prPrec i 0 (concatD [prt 0 type' , prt 0 id])


instance Print Block where
  prt i e = case e of
   Blocks decls stmts -> prPrec i 0 (concatD [doc (showString "{") , prt 0 decls , prt 0 stmts , doc (showString "}")])


instance Print Decl where
  prt i e = case e of
   DeclAssign arg expression -> prPrec i 0 (concatD [prt 0 arg , doc (showString "=") , prt 0 expression])
   Decls arg -> prPrec i 0 (concatD [prt 0 arg])

  prtList es = case es of
   [] -> (concatD [])
   x:xs -> (concatD [prt 0 x , doc (showString ";") , prt 0 xs])

instance Print Stmt where
  prt i e = case e of
   StIf expr block -> prPrec i 0 (concatD [doc (showString "if (") , prt 0 expr , doc (showString ")") , prt 0 block])
   StIfEl expr block0 block -> prPrec i 0 (concatD [doc (showString "if (") , prt 0 expr , doc (showString ")") , prt 0 block0 , doc (showString "else") , prt 0 block])
   StWhile expr block -> prPrec i 0 (concatD [doc (showString "while (") , prt 0 expr , doc (showString ")") , prt 0 block])
   StFor decl expr assignment block -> prPrec i 0 (concatD [doc (showString "for") , doc (showString "(") , prt 0 decl , doc (showString ";") , prt 0 expr , doc (showString ";") , prt 0 assignment , doc (showString ")") , prt 0 block])
   StAssi assignment -> prPrec i 0 (concatD [prt 0 assignment , doc (showString ";")])
   StPrintExpr expr -> prPrec i 0 (concatD [doc (showString "print") , doc (showString "(") , prt 0 expr , doc (showString ")") , doc (showString ";")])

  prtList es = case es of
   [] -> (concatD [])
   x:xs -> (concatD [prt 0 x , prt 0 xs])

instance Print Expression where
  prt i e = case e of
   Exprs expr -> prPrec i 0 (concatD [prt 0 expr])


instance Print Expr where
  prt i e = case e of
   EOr expr0 expr -> prPrec i 0 (concatD [prt 0 expr0 , doc (showString "||") , prt 1 expr])
   EAnd expr0 expr -> prPrec i 0 (concatD [prt 0 expr0 , doc (showString "&&") , prt 1 expr])
   EEq expr0 expr -> prPrec i 1 (concatD [prt 1 expr0 , doc (showString "==") , prt 2 expr])
   ENEq expr0 expr -> prPrec i 1 (concatD [prt 1 expr0 , doc (showString "!=") , prt 2 expr])
   ELt expr0 expr -> prPrec i 2 (concatD [prt 2 expr0 , doc (showString "<") , prt 3 expr])
   EGt expr0 expr -> prPrec i 2 (concatD [prt 2 expr0 , doc (showString ">") , prt 3 expr])
   EAdd expr0 expr -> prPrec i 3 (concatD [prt 3 expr0 , doc (showString "+") , prt 4 expr])
   ESub expr0 expr -> prPrec i 3 (concatD [prt 3 expr0 , doc (showString "-") , prt 4 expr])
   EMul expr0 expr -> prPrec i 4 (concatD [prt 4 expr0 , doc (showString "*") , prt 5 expr])
   EDiv expr0 expr -> prPrec i 4 (concatD [prt 4 expr0 , doc (showString "/") , prt 5 expr])
   ENeg expr -> prPrec i 5 (concatD [doc (showString "-") , prt 6 expr])
   EVar id -> prPrec i 6 (concatD [prt 0 id])
   EInt n -> prPrec i 6 (concatD [prt 0 n])
   ETrue  -> prPrec i 6 (concatD [doc (showString "true")])
   EFalse  -> prPrec i 6 (concatD [doc (showString "false")])


instance Print Assignment where
  prt i e = case e of
   Assign id expression -> prPrec i 0 (concatD [prt 0 id , doc (showString "=") , prt 0 expression])
   AOper id opassign expression -> prPrec i 0 (concatD [prt 0 id , prt 0 opassign , prt 0 expression])
   AIncrDecr id incrdecr -> prPrec i 0 (concatD [prt 0 id , prt 0 incrdecr])


instance Print OpAssign where
  prt i e = case e of
   OpAPlus  -> prPrec i 0 (concatD [doc (showString "+=")])
   OpAMinus  -> prPrec i 0 (concatD [doc (showString "-=")])
   OpAMult  -> prPrec i 0 (concatD [doc (showString "*=")])
   OpADiv  -> prPrec i 0 (concatD [doc (showString "/=")])


instance Print IncrDecr where
  prt i e = case e of
   Increment  -> prPrec i 0 (concatD [doc (showString "++")])
   Decrement  -> prPrec i 0 (concatD [doc (showString "--")])


instance Print Type where
  prt i e = case e of
   TInt  -> prPrec i 0 (concatD [doc (showString "int")])
   TBool  -> prPrec i 0 (concatD [doc (showString "bool")])



