module AbsData where

import Data.Map as M
import Control.Monad.Reader
import Control.Monad.State
import Absgramatyka

type Loc = Integer -- lokacja
type Env = M.Map String Loc -- środowisko
type Stan  = M.Map Loc Integer  -- stan

emptyEnv :: Env
emptyEnv = M.empty

-- na pozycji 0 zapisany jest numer następnej wolnej lokacji
initialSt :: Stan
initialSt = M.singleton 0 1

type Semantics a = ReaderT Env (StateT Stan IO) a
-- albo StateT St (Reader Env) a
-- albo Monada = ReaderT Env (StateT St Identity)

takeLocation :: Ident -> Semantics Loc
takeLocation ident = do
  Just loc <- asks (M.lookup (evalIdent ident))
  return loc

takeValue :: Loc -> Semantics Integer
takeValue loc = do
  Just val <- gets (M.lookup loc)
  return val

evalIdent :: Ident -> String
evalIdent (Ident str) = str