module Declarations where

import Data.Map as M
import Control.Monad.Reader
import Control.Monad.State
import AbsData
import Expressions
import Absgramatyka

evalDecl :: Decl -> Semantics Env
evalDecl (Decls (Args typ ident)) = do
  Just newLoc <- gets (M.lookup 0)
  modify (M.insert newLoc 0)
  modify (M.insert 0 (newLoc+1))
  env <- ask
  return $ M.insert (evalIdent ident) newLoc env
evalDecl (DeclAssign (Args typ ident) expr) = do
  Just newLoc <- gets (M.lookup 0)
  val <- evalEx expr
  modify (M.insert newLoc val)
  modify (M.insert 0 (newLoc+1))
  env <- ask
  return $ M.insert (evalIdent ident) newLoc env

-- Dodatkowa zabawa, aby druga deklaracja z listy mogla juz uzywac pierwszej zmiennej
evalDeclarations :: [Decl] -> Semantics Env
evalDeclarations [] = ask
evalDeclarations (decl:decls) = do
  env' <- evalDecl decl
  local (const env') (evalDeclarations decls)