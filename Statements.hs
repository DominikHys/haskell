module Statements where

import Data.Map as M
import Control.Monad.Reader
import Control.Monad.State
import Assignments
import Declarations
import AbsData
import Absgramatyka
import Expressions

-- FOR1 - sprawdza warunek (nowa funkcja żeby widział już tę nowoutworzoną zmienną z wnętrza fora)
evalAndDoFor :: Expr -> Assignment -> Block -> Semantics ()
evalAndDoFor bexpr assignment block@(Blocks decls stmts) = do
  val_bexpr <- evalEx (Exprs bexpr)
  if val_bexpr == 0
    then return ()
    else do
      doFor bexpr assignment block

-- FOR 2 - blok do zrobienia, warunek boolowy, ktory musi zachodzic na koncu i co robić po każdym obrocie
doFor :: Expr -> Assignment -> Block -> Semantics () 
doFor bexpr assignment block@(Blocks decls stmts) = do
  env' <- evalDeclarations decls
  local (const env') (doAndCheckFor bexpr assignment block)

--FOR 3 - deklaracje zrobione, obrobić wnętrze blocku, zrobić końcowego assignmenta i sprawdzić warunek
doAndCheckFor :: Expr -> Assignment -> Block -> Semantics ()
doAndCheckFor bexpr assignment block@(Blocks decls stmts) = do
  mapM_ interpret stmts
  interpretAssi assignment
  val_bexpr <- evalEx (Exprs bexpr)
  if val_bexpr == 0
    then return ()
    else doFor bexpr assignment block

interpret :: Stmt -> Semantics ()
interpret (StAssi assi) = do --assignment
  interpretAssi assi

interpret this@(StWhile bexpr block) = do --while
  bval <- eval bexpr
  if bval == 0
     then return ()
     else do
       interpretBl block
       interpret this

interpret (StFor decl bexpr assignment block) = do --for
  env' <- evalDecl decl
  local (const env') (evalAndDoFor bexpr assignment block)

interpret (StIf bexpr block) = do --if
  bval <- eval bexpr
  interpretBl block
interpret (StIfEl bexpr block1 block2) = do --if else
  bval <- eval bexpr
  if bval == 0
    then interpretBl block1
    else interpretBl block2

interpret (StPrintExpr ETrue) = do
  liftIO $ print "true"
interpret (StPrintExpr EFalse) = do
  liftIO $ print "false"
interpret (StPrintExpr expr) = do --print expr
  val <- evalEx (Exprs expr)
  liftIO $ print val

interpretBl :: Block -> Semantics ()
interpretBl (Blocks decls stmts) = do
  env' <- evalDeclarations decls
  local (const env') (mapM_ interpret stmts)